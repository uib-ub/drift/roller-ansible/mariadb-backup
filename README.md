MariaDB backup role
=========

A role which creates and configures a backup user and cron job for that user, to store backups using [mysqldump](https://mariadb.com/kb/en/mysqldump/). Configures a home dir for the user with a [my.cnf](https://mariadb.com/kb/en/configuring-mariadb-with-option-files/#default-option-file-locations-on-linux-unix-mac) with authentication details for running the dump. By default files older than 31 days are deleted, unless they are from the first of each month. ..01-12-2012..

Requirements
------------

Requires a working mariadb installation and mail to work from cron (for sending out error on cron). The credentials for the mariadb-user should also have been created and set before setting up the service. If the default filenames are changed, then the logic for keeping one backup per month will also need to be fixed.

Role Variables
--------------

mariadb_cron_delete_old_files
A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
